package open.fontscaling;

import android.content.Context;

import static android.content.Context.MODE_PRIVATE;

public class SharePrefUtil {

    public static final String FONT_SCALING_FILE_NAME = "fontscaling";
    public static final String TEXT_SIZE_SCALING_SHARED_PREF_KEY = "TEXT_SIZE_SCALING";

    public static float getTextSizeScaling(Context context) {
        return context.getSharedPreferences(FONT_SCALING_FILE_NAME, MODE_PRIVATE)
                .getFloat(TEXT_SIZE_SCALING_SHARED_PREF_KEY, 1f);
    }

    public static void saveTextSizeScaling(float scale, Context context) {
        context.getSharedPreferences(FONT_SCALING_FILE_NAME, MODE_PRIVATE)
                .edit()
                .putFloat(TEXT_SIZE_SCALING_SHARED_PREF_KEY, scale)
                .apply();
    }
}
