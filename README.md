**Dynamic font scale**

This library can impart dynamic font scale feature into a given android app. Meaning, user can
choose to scale up or down the font size in an app

**Gradle usage**

In your main gradle file under repositories add

`maven { url 'https://jitpack.io' }`
In your apps gradle file add the below dependency

```implementation 'com.gitlab.sultanahamer:FontScalingAndroid:-SNAPSHOT'```

**Usage in app**

If you have been maintaining Android app having multiple themes, you might have had to implement yourself
a App level BaseActivity and BaseFragment. In case not yet, it is good to have your base Activity across
application. Having a base activity lets you impart some defaults across app.

To use this library, follow the example app present in app directory. It has one single activity mimicking
app preferences.

Major takeaways are

1. add this library to your app's build.gradle as shown in gradle usage above
2. `setCustomFontSizeOnViewCreated(this)` to be called from AppbaseActivity or activity that requires dynamic font scale
3. From your preferences screen, add setOnPreferenceClickListener as below
```
fontScalPreference.setOnPreferenceClickListener(preference -> {
   new FontScalePreferenceHandler(activity).open();
   return true;
});
```