package com.example.dynamicfontscale;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import open.fontscaling.FontScalePreferenceHandler;

import static open.fontscaling.SharePrefUtil.TEXT_SIZE_SCALING_SHARED_PREF_KEY;

public class PreferencesActivity extends AppBaseActivity {
    private static final String PREFERENCE_FRAGMENT_TRANSACTION_TAG = "fragment_tag";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        if(!supportFragmentManager.getFragments().isEmpty()) return;
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, new PreferencesFragment(), PREFERENCE_FRAGMENT_TRANSACTION_TAG)
                .commit();
    }

    @Override
    int getLayoutResource() {
        return R.layout.activity_preferences;
    }


    public static class PreferencesFragment extends PreferenceFragmentCompat {
        Activity activity = null;

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            activity = getActivity();
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            getPreferenceManager().setSharedPreferencesName("somename");
            addPreferencesFromResource(R.xml.preferences);
            setupFontScalePreference();
        }

        private void setupFontScalePreference() {
            Preference fontScalPreference = findPreference(TEXT_SIZE_SCALING_SHARED_PREF_KEY);
            fontScalPreference.setOnPreferenceClickListener(preference -> {
                new FontScalePreferenceHandler(activity).open();
                return true;
            });
        }
    }

}
