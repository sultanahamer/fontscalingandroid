package com.example.dynamicfontscale;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import static open.fontscaling.FontScalingUtil.setCustomFontSizeOnViewCreated;

public abstract class AppBaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setCustomFontSizeOnViewCreated(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayoutResource());
        super.onCreate(savedInstanceState);
    }

    abstract int getLayoutResource();

}
